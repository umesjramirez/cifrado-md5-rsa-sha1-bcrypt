<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.6.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.6.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/logo4.png" type="image/x-icon">
  <meta name="description" content="Web Site Creator Description">
  <title>login</title>
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
  <section id="section-rsa" class="mbr-section form4 cid-rnbuS7vLeZ" >

    

    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="google-map">
                <div class="input-group-btn col-md-12" style="margin-top: 10px;"><a href="index.php" type="submit" class="btn btn-danger btn-form display-4">REGRESAR</a></div>
                </div>
            </div>
            <div class="col-md-8">
                <?php if (isset($_GET['error'])) { ?>
                    <h2  style="color:red" ><div id="error_div" style="display:block"><?php echo 'Nombre de usuario o contraseña incorrecto(s)'; ?></div></h2>    
                 <?php } ?>
                <h2 class="pb-3 align-left mbr-fonts-style display-2">
                    RSA</h2>
                <div>
                    <div class="icon-block pb-3">
                        <span class="icon-block__icon">
                            <span class="mbr-iconfont mbri-user"></span>
                        </span>
                        <h4 class="icon-block__title align-left mbr-fonts-style display-5">
                            Login</h4>
                    </div>
                    
                </div>
                <!-- <div data-form-type="formoid"> -->
                <div >
                    <div data-form-alert="" hidden="">
                        Gracias por ingresar su información!
                    </div>
                    <?php if (!isset($_GET['entro'])) { ?>
                    <form action = "rsa-1.php" method = "post">
                        <div class="row">
                            <div class="col-md-12 multi-horizontal" data-for="name">
                                <input type="text" class="form-control input" name="username" data-form-field="username" placeholder="Nombre de usuario" required="" id="username" onfocus="limpiaDiv()">
                            </div>
                            <div class="col-md-12" data-for="password">
                                <input type="password" class="form-control input" name="password" data-form-field="password" placeholder="Contraseña" required="" id="password">
                            </div>
                            
                            <div class="input-group-btn col-md-12" style="margin-top: 10px;">
                                <input class="btn btn-primary btn-form display-4" type="submit" value = " Entrar "/><br />
                                <!-- <button  type="submit" class="btn btn-primary btn-form display-4">ENTRAR</button> -->
                            </div>
                        </div>
                    </form>
                    <?php } else { ?>
                        <div >
                            <h2 style="color:green">Exito, credenciales correctas</h2>
                            <h3>Query: </h3>
                            <?php echo $_GET['entro']; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>


  <section class="engine"><a href="https://mobirise.ws/j">design your own website</a></section><script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/formoid/formoid.min.js"></script>
  <script type="text/javascript">
        // $(document).ready(function() {
        //     $('#username').focusin(function(e) { 
        //         $('#error_div').hide();
        //     )};
        // )};
        function limpiaDiv ()
        {
            <?php if (isset($_GET['error'])) { ?>
            document.getElementById('error_div').style.display='none';
            <?php } ?>
        }
</script>
  
  
</body>
</html>