<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.6.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.6.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/logo4.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Home</title>
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
  <section class="header12 cid-rnbrEu1JWz mbr-fullscreen mbr-parallax-background" id="header12-j">

    

    <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(5, 93, 51);">
    </div>

    <div class="container  ">
            <div class="media-container">
                <div class="col-md-12 align-center">
                    <h1 class="mbr-section-title pb-3 mbr-white mbr-bold mbr-fonts-style display-1">CIFRADO DE CADENA DE CONEXION</h1>
                    <ul>
                        <li class="mbr-white mbr-fonts-style display-5">JULIO RENE RAMIREZ / CARNE 201008138</li>
                        <li class="mbr-white mbr-fonts-style display-5">EMERSON VELASQUEZ / CARNE 201608058</li>
                    </ul>
                    
                    <div class="mbr-section-btn align-center py-2"><a class="btn btn-md btn-success display-4" href="test.php">REPORTE</a></div>

                    <div class="icons-media-container mbr-white">
                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                            <a href="md5.php">
                                <span class="mbr-iconfont socicon-modelmayhem socicon"></span>
                            </a>
                            </div>
                            <h5 class="mbr-fonts-style display-5">MD5</h5>
                        </div>

                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                                <a href="bcrypt.php">
                                    <span class="mbr-iconfont socicon-blogger socicon"></span>
                                </a>
                            </div>
                            <h5 class="mbr-fonts-style display-5">
                                BCRYPT</h5>
                        </div>

                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                                <a href="sha1.php">
                                    <span class="mbr-iconfont socicon-istock socicon"></span>
                                </a>
                            </div>
                            <h5 class="mbr-fonts-style display-5">
                                SHA1</h5>
                        </div>

                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                                <a href="rsa.php">
                                    <span class="mbr-iconfont socicon-formulr socicon"></span>
                                </a>
                            </div>
                            <h5 class="mbr-fonts-style display-5">RSA</h5>
                        </div>
                        <div class="card col-12 col-md-6 col-lg-3">
                            <div class="icon-block">
                                <a href="insert.php">
                                    <span class="mbr-iconfont socicon-formulr socicon"></span>
                                </a>
                            </div>
                            <h5 class="mbr-fonts-style display-5">INSERT</h5>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    
</section>


  <section class="engine"><a href="https://mobirise.ws/c">best free site maker</a></section><script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/parallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  
  
</body>
</html>