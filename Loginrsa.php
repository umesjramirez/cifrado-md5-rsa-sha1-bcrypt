<?php 



    // function __construct() 
    // {
    //     parent::__construct();
    //     $this->load->model('crud_model');
    //     $this->load->database();
    //     $this->load->library('session');
    //     $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
    //     $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    //     $this->output->set_header('Pragma: no-cache');
    //     $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
    // }

    function encrypt($data)
    {
      $pubkey = '-----BEGIN PUBLIC KEY-----
      MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCynBpYR/1LAY5+qQKjpclMV9wX
      jr3JF4GG01WkelXg9jiDaaHTxqjH6dhCbfxctlBZG0JxBs2MD0NHDn1FW/m8sqKz
      apjKG8TjE5l+dTdGS52TuMpDZeEkXBnHaRcyuoXg+rEp+anElUs+9xHrDM/F6f1i
      +/OkyqvGdKt5/x1aIQIDAQAB
      -----END PUBLIC KEY-----';
      $privkey = '-----BEGIN RSA PRIVATE KEY-----
      MIICXQIBAAKBgQCynBpYR/1LAY5+qQKjpclMV9wXjr3JF4GG01WkelXg9jiDaaHT
      xqjH6dhCbfxctlBZG0JxBs2MD0NHDn1FW/m8sqKzapjKG8TjE5l+dTdGS52TuMpD
      ZeEkXBnHaRcyuoXg+rEp+anElUs+9xHrDM/F6f1i+/OkyqvGdKt5/x1aIQIDAQAB
      AoGATZAUsLmyeVwXsVFb4A0iuZB994z5RdZ7XT1xN5gYqzTBne/FYt4mdYpLa62v
      peNWSRvZYpP9txD5umXEaNZkQJ6K4ty1e/cXyHEzUTUMbCKzoO1/Cuul/gvDKEui
      Wh7MkYjgS8N3ACFM0fGKl3gfP7rcfTx0MV0EYjllY7/92XECQQDxduYXuKaYAVX7
      V5fqUIdonJUHrc7yTs/ZKmHked/fjBplFm1OWv5wngrnv417O8cJo1O3Rzt0xOts
      WWbX0PgbAkEAvVyUJ5aNiyRg3bg32tXAle+n3He7tErAAUBQ1xf2/Lytmd6gSGiu
      +B/X7dG6DLbco93tOO5Y2H6gntRr1SQScwJAWNSzBVsK/rTPGu44lx0VE6k3MmIA
      1iekU3JCHTmw4J68PyAnCkJzhJ6/yE4yEHoHWUZaz/xGpUiki3HAYfkgrQJBALKt
      O4LLVY24asprfBC6ZvlTZJHsTKpHajsEBAOhskUyh6BL5/PLC2ZHLclSfvpmRXU+
      AAwbIqxNd6PHDmTUn68CQQCXkqnvyvazT8TwQTQR4QiGxORftfX3/DPYfhLum0+V
      ryco4HeXS96Byhx4AWUQoqdN8iSWd9VavcWYK7/dcFIo
      -----END RSA PRIVATE KEY-----';
      if (openssl_public_encrypt($data, $encrypted, $pubkey))
      {
        $data = base64_encode($encrypted);
      }
      else
      {
        throw new Exception('Unable to encrypt data. Perhaps it is bigger than the key size?');
      }
      return $data;
    }

    function decrypt($data)
    {
      $pubkey = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjfmgaAnU5yhja6yfrQ9+phH/r
6e0w9B+y7bcqtnnqTnUvPmXbRzACzlHn7bvQgVJPJbr06g8isHVEYpbVOZGj1ryL
O+YekmKOSpqx26Qyb+8eLjtyawpMu3koDtCyB8oAL+4JgECFV5R0ZrpprH6TrW6I
KFligiEeDC6sLBsNgwIDAQAB
-----END PUBLIC KEY-----';
      $privkey = '-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCjfmgaAnU5yhja6yfrQ9+phH/r6e0w9B+y7bcqtnnqTnUvPmXb
RzACzlHn7bvQgVJPJbr06g8isHVEYpbVOZGj1ryLO+YekmKOSpqx26Qyb+8eLjty
awpMu3koDtCyB8oAL+4JgECFV5R0ZrpprH6TrW6IKFligiEeDC6sLBsNgwIDAQAB
AoGAb4WqPROwIttYWPxPHowN5PQczi+jO+FKPiMo7lnKj6k1BiTqFclZVpCmDi58
Dp4oJxT1klqWOHNlimA1wvyh+qh96NqMo6tqhXY0b6HRYUGlUXSftE7TA44VOce3
Rd+2EAPBfsuSoOy/nfO3NeW9oyNv6d6dPzX+z0HkMcAL9QECQQDcbuPD5f6PmLye
k7jFwpd0YLD5q2DzpqUgvGylAVaCZsq4HBcc6hvoWZKgeMdJCyCSka0Y2Q7vtsaB
n728dRh/AkEAvd+a3iF8mPjRTSOGyxwLY5rHNzaXVrqX1rOGSdOtfD+Rfq7udwdc
D5IMLXzzxNP4sBorDro+8BFAoYlSSHIo/QJBANMffhSVhw+fmSBEmdcHPiofpcEr
DGuwsz08Ws5bDrFMpGJbvSNbcilDCTWbGDv0ZVkcH27KDg03wL5L8dy7Rh8CQDOh
gz5WAsiiGGAOgg/nD13Fb9ieuqZ11OYoKSMYMXuw2YKGVQf+81S0lSUlbjAep+hT
JVQ65vRm7JlAeySCDo0CQFqGkV1rja1Pbh17jFPkhDf4tqH1lFFNh9m8KRNJkCo5
Rm0tqT1bllkwPI/XS8qyDW4n4lCFeVM5edSbH5xnTrs=
-----END RSA PRIVATE KEY-----';
      if (openssl_private_decrypt(base64_decode($data), $decrypted, $privkey))
      {
        $data = $decrypted;
      }
      else
      {
        $data = '';
      }
      return $data;
    }

    echo encrypt('password');
    exit();

    function getPass($username) {
      include("config.php");
      $sql = "SELECT rsa_pass FROM users WHERE username = '$username' ";
      $result = mysqli_query($db,$sql);
      if (!$db)
      {
          echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
          echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL;
          echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
          return false;
        }
        
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      if ($row['rsa_pass'])
      {
          return $row['rsa_pass'];
      } 
      else { 
          return false;
      } 
  }


    if($_SERVER["REQUEST_METHOD"] == "POST") {
        // username and password sent from form 
      //   echo $_POST['username'].' - '.$_POST['password'];
      //   $myusername = mysqli_real_escape_string($db,$_POST['username']);
      //   $mypassword = mysqli_real_escape_string($db,$_POST['password']); 

        $myusername = $_POST['username'];
        $mypassword = $_POST['password']; 

    
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $pass = getPass($username); ?>

      <html>
   
        <head>
           <title>RSA</title>
        </head>
        
        <body>        
        <?php
      
        if($this->decrypt($pass) == $mypassword)
        { ?>
          <br><br>
          <h1>Welcome <?php echo $myusername; ?></h1> 
          <h2><a href = "logout.php">Sign Out</a></h2>
        <?php }
        else 
        { ?>
            <h2>CONTRASEÑA INVALIDA</h2>
            <!-- <h2><a href = "logout.php">Sign Out</a></h2> -->
            <h2><a href="login.php">Regresar a home</a></h2>
            <?php 
        }
      
    }
    