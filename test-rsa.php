<?php
include("crypt.php");
// generate private/public key as follows:
// > openssl genrsa -out private.pem 2048
// > openssl rsa -in private.pem -outform PEM -pubout -out public.pem

$data = "password";


$privKey = openssl_pkey_get_private('file://keys/private.pem');
echo 'encontro privada '.$privKey.'<br>';
$encryptedData = "";
openssl_private_encrypt($data, $encryptedData, $privKey);
echo 'Encrypted: ' . $encryptedData.'<br>';
echo 'Encrypted1: ' . base64_encode($encryptedData).'<br>';

$pubKey = openssl_pkey_get_public('file://keys/public.pem');
echo 'encontro publica '.$pubKey.'<br>';
$decryptedData = "";
openssl_public_decrypt(($encryptedData), $decryptedData, $pubKey);
echo "\n---\nDecrypted: " . $decryptedData.'<br>';

echo "\n[OK]\n";