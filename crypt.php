<?php 
function encrypt($data)
    {
        // Obtenemos la llave privada
        $privKey = openssl_pkey_get_private('file://keys/private.pem');
        // echo 'encontro privada '.$privKey.'<br>';
        if ($privKey)
        {
            $encryptedData = "";
            if (openssl_private_encrypt($data, $encryptedData, $privKey)){
            // echo 'Encrypted: ' . $encryptedData.'<br>';
                return base64_encode($encryptedData);
            }
            else 
            {
                return false;
            }
        }
        else 
            {
                return false;
            }
    }


    function decrypt($encryptedData)
    {
        $pubKey = openssl_pkey_get_public('file://keys/public.pem');
        // echo 'encontro publica '.$pubKey.'<br>';
        if ($pubKey)
        {
            $decryptedData = "";
            if (openssl_public_decrypt($encryptedData, $decryptedData, $pubKey)){
                return $decryptedData;
            }
            else {
                return false;
            }
            
        }
        else {
            return false;
        }
        
    }