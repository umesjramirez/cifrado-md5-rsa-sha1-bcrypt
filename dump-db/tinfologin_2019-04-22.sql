# ************************************************************
# Sequel Pro SQL dump
# Version 5438
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.33-0ubuntu0.14.04.1)
# Database: tinfologin
# Generation Time: 2019-04-22 07:35:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(60) DEFAULT NULL,
  `md5_pass` varchar(60) DEFAULT NULL,
  `rsa_pass` varchar(345) DEFAULT NULL,
  `bcrypt_pass` varchar(60) DEFAULT NULL,
  `sha1_pass` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `username`, `md5_pass`, `rsa_pass`, `bcrypt_pass`, `sha1_pass`)
VALUES
	(1,'Julio Ramirez','jramirez','5f4dcc3b5aa765d61d8327deb882cf99','PHfZgkuiBPG7AEcfhT+Qg3R47zRoV8ajKWZNvWIDAqmCiBikbmVN0oTUzzNLEfHsZZ9wV6UwkP70ml1I9Y/9SXQpZU1ugb4vzZeyXd7wFg4COQVcTCtrPE/5Nz+2+TO9lO2BDiR+IUuf4G3ulPDXfohbYzU1nhnYiPGq0OegYhoF+20i4EkuizVAVfcEd0ylOFPiWH9lXBiz2lm18gA6c9N7kBMDcz9O/bli217Jj3UwbnGamPLgi7jBkCuuwr8WZe0c9CuXkZUtJLjGUNXZMrEsTSiYIgWqeBOgw+628JFPgrqLXupNygf2rN4Rc4j54qr2xkCHYVauQqZG5nX6zA==','$2y$10$cGHzwbezmO5LXCI1hHRBgu6fXmmDXOVWM662IhDPqCkkZYYQ/5hJ.','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
